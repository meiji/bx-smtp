<?php

namespace Meiji\BxSmtp;

use PHPMailer\PHPMailer\PHPMailer;
use Bitrix\Main\Config\Option;


class Mailer extends \Meiji\BxMarketplace\Fixtures\AbstractSolution implements
	\PetrKnap\Php\Singleton\SingletonInterface
{
	
	use \PetrKnap\Php\Singleton\SingletonTrait;
	
	const BX_INTRODUCE_NAME = 'meiji_smtp';
	
	protected $smtp, $smtpHost, $smtpPort, $smtpSecure, $smtpAuth, $smtpLogin, $smtpPassword, $smtpAutoTLS, $fromMail, $fromName;
	
	private function __construct()
	{
		
		$this->smtp         = Option::get(self::BX_INTRODUCE_NAME, 'smtp', 'N') == 'Y' ? true : false;
		$this->smtpHost     = Option::get(self::BX_INTRODUCE_NAME, 'smtpHost', '');
		$this->smtpPort     = Option::get(self::BX_INTRODUCE_NAME, 'smtpPort', '25');
		$this->smtpAutoTLS  = Option::get(self::BX_INTRODUCE_NAME, 'smtpAutoTLS', 'Y') == 'Y' ? true : false;
		$this->smtpSecure   = Option::get(self::BX_INTRODUCE_NAME, 'smtpSecure', '');
		$this->smtpAuth     = Option::get(self::BX_INTRODUCE_NAME, 'smtpAuth', 'Y') == 'Y' ? true : false;
		$this->smtpLogin    = Option::get(self::BX_INTRODUCE_NAME, 'smtpLogin', '');
		$this->smtpPassword = Option::get(self::BX_INTRODUCE_NAME, 'smtpPassword', '');
		$this->fromName     = Option::get(self::BX_INTRODUCE_NAME, 'fromName', '');
		$this->fromMail     = Option::get(self::BX_INTRODUCE_NAME, 'fromMail', '');
	}
	
	public static function getAdminFields()
	{
		
		$Mailer = self::getInstance();
		
		$arFields = [
			'smtp'         => [
				'NAME'   => 'Использовать SMTP',
				'TYPE'   => 'C',
				'ACTIVE' => $Mailer->smtp
			],
			'smtpHost'     => [
				'NAME'  => 'SMTP Хост',
				'TYPE'  => 'S',
				'VALUE' => $Mailer->smtpHost
			],
			'smtpPort'     => [
				'NAME'  => 'SMTP Порт',
				'TYPE'  => 'S',
				'VALUE' => $Mailer->smtpPort
			],
			'smtpAutoTLS'  => [
				'NAME'   => 'Автовыбор сертификата',
				'TYPE'   => 'C',
				'ACTIVE' => $Mailer->smtpAutoTLS
			],
			'smtpSecure'   => [
				'NAME'   => 'SMTP Сертификат',
				'TYPE'   => 'L',
				'VALUES' => [
					[
						'VALUE'  => 'Без шифрования',
						'VAR'    => '',
						'ACTIVE' => $Mailer->smtpSecure == ''
					],
					[
						'VALUE'  => 'SSL',
						'VAR'    => 'ssl',
						'ACTIVE' => $Mailer->smtpSecure == 'ssl'
					],
					[
						'VALUE'  => 'TLS',
						'VAR'    => 'tls',
						'ACTIVE' => $Mailer->smtpSecure == 'tls'
					]
				]
			],
			'smtpAuth'     => [
				'NAME'   => 'SMTP требует авторизацию',
				'TYPE'   => 'C',
				'ACTIVE' => $Mailer->smtpAuth
			],
			'smtpLogin'    => [
				'NAME'  => 'SMTP Логин',
				'TYPE'  => 'S',
				'VALUE' => $Mailer->smtpLogin
			],
			'smtpPassword' => [
				'NAME'   => 'SMTP Пароль',
				'TYPE'   => 'S',
				'S_TYPE' => 'password',
				'VALUE'  => $Mailer->smtpPassword
			],
			'fromName'     => [
				'NAME'  => 'Имя отправтиеля',
				'TYPE'  => 'S',
				'VALUE' => $Mailer->fromName
			],
			'fromMail'     => [
				'NAME'  => 'E-mail отправителя (если поддерживается)',
				'TYPE'  => 'S',
				'VALUE' => $Mailer->fromMail
			],
		];
		
		return $arFields;
	}
	
	public static function setAdminFields($arFields)
	{
		
		if (array_key_exists('smtp', $arFields) && $arFields['smtp'] == 'Y') {
			Option::set(self::BX_INTRODUCE_NAME, 'smtp', 'Y');
		} else {
			Option::set(self::BX_INTRODUCE_NAME, 'smtp', 'N');
		}
		
		if (array_key_exists('smtpHost', $arFields)) {
			Option::set(self::BX_INTRODUCE_NAME, 'smtpHost', $arFields['smtpHost']);
		}
		
		if (array_key_exists('smtpPort', $arFields)) {
			Option::set(self::BX_INTRODUCE_NAME, 'smtpPort', $arFields['smtpPort']);
		}
		
		if (array_key_exists('smtpAutoTLS', $arFields) && $arFields['smtpAutoTLS'] == 'Y') {
			Option::set(self::BX_INTRODUCE_NAME, 'smtpAutoTLS', 'Y');
		} else {
			Option::set(self::BX_INTRODUCE_NAME, 'smtpAutoTLS', 'N');
		}
		
		if (array_key_exists('smtpSecure', $arFields)) {
			Option::set(self::BX_INTRODUCE_NAME, 'smtpSecure', $arFields['smtpSecure']);
		}
		
		if (array_key_exists('smtpAuth', $arFields) && $arFields['smtpAuth'] == 'Y') {
			Option::set(self::BX_INTRODUCE_NAME, 'smtpAuth', 'Y');
		} else {
			Option::set(self::BX_INTRODUCE_NAME, 'smtpAuth', 'N');
		}
		
		if (array_key_exists('smtpLogin', $arFields)) {
			Option::set(self::BX_INTRODUCE_NAME, 'smtpLogin', $arFields['smtpLogin']);
		}
		
		if (array_key_exists('smtpPassword', $arFields)) {
			Option::set(self::BX_INTRODUCE_NAME, 'smtpPassword', $arFields['smtpPassword']);
		}
		
		if (array_key_exists('fromName', $arFields)) {
			Option::set(self::BX_INTRODUCE_NAME, 'fromName', $arFields['fromName']);
		}
		
		if (array_key_exists('fromMail', $arFields)) {
			Option::set(self::BX_INTRODUCE_NAME, 'fromMail', $arFields['fromMail']);
		}
		
		return;
	}
	
	public static function send($to, $subject, $message, $additional_headers, $additional_parameters, $context)
	{
		
		$Self = self::getInstance();
		$mail = new PHPMailer();
		
		$mail->CharSet = 'UTF-8';
		$mail->setLanguage('ru');
		$mail->isHTML(true);
		
		if ($Self->smtp) {
			$mail->isSMTP();
			$mail->Host = $Self->smtpHost;
			$mail->Port = $Self->smtpPort;
			if (empty($Self->smtpSecure)) {
				$mail->SMTPAutoTLS = $Self->smtpAutoTLS;
			} else {
				$mail->SMTPSecure = $Self->smtpSecure;
			}
			if ($Self->smtpAuth) {
				$mail->SMTPAuth = true;
				$mail->Username = $Self->smtpLogin;
				$mail->Password = $Self->smtpPassword;
			}
		}
		
		if (!empty($Self->fromMail)) {
			$mail->From = $Self->fromMail;
		}
		
		if (!empty($Self->fromName)) {
			$mail->FromName = $Self->fromName;
		}
		
		$mail->Subject = $subject;
		$mail->Body    = $message;
		
		$arRecipients = $mail->parseAddresses($to);
		foreach ($arRecipients as $recipient) {
			$mail->addAddress($recipient['address'], $recipient['name']);
		}
		
		$arr = explode("\n", $additional_headers);
		if (is_array($arr)) {
			foreach ($arr as $key => $value) {
				$arrr               = explode(":", $value);
				$additional_headers = $mail->headerLine($arrr[0], $arrr[1]);
				
				if ($arrr[0] == 'Content-Type') $mail->ContentType = $arrr[1];
				
				if ($arrr[0] == 'Reply-To') $mail->addReplyTo($arrr[1]);
				
				if ($arrr[0] == 'CC') {
					$arCC = $mail->parseAddresses($arrr[1]);
					foreach ($arCC as $ccMail) {
						$mail->addCC($ccMail['address'], $ccMail['name']);
					}
				}
				
				if ($arrr[0] == 'From') {
					$arFrom = $mail->parseAddresses($arrr[1]);
					foreach ($arFrom as $from) {
						if (empty($from['name'])) {
							$from['name'] = '';
						}
						$mail->setFrom($from['address'], $from['name']);
					}
				}
				
				if ($arrr[0] == 'BCC') {
					$arBCC = $mail->parseAddresses($arrr[1]);
					foreach ($arBCC as $bccMail) {
						$mail->addBCC($bccMail['address'], $bccMail['name']);
					}
				}
			}
		}
		
		
		//$mail->SMTPDebug = 2;
		
		if (!$mail->send()) {
			//echo "Mailer Error: " . $mail->ErrorInfo;
			
			return false;
		} else {
			//echo "Message sent!";
			
			return true;
		}
		
	}
}
