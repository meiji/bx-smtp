<?
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$aAdminMenuLinks = [
	'meiji_smtp' => [
		'parent_menu' => 'global_menu_settings',
		'sort'        => 30,
		'text'        => Loc::getMessage('MEIJI_SOLUTION_SMTP_ADMINMENU_ITEM_TEXT'),
		'title'       => Loc::getMessage('MEIJI_SOLUTION_SMTP_ADMINMENU_ITEM_TITLE'),
		'icon'        => 'sender_menu_icon'
	]
];
