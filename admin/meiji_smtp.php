<?

use Bitrix\Main\Localization\Loc;


require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php');

Loc::loadMessages(__FILE__);

$request = \Bitrix\Main\Context::getCurrent()->getRequest();

if ($request->getPost('apply')) {
	
	$arNewSettings = $request->get('meiji_smtp');
	if (!empty($arNewSettings)) {
		\Meiji\BxSmtp\Mailer::setAdminFields($arNewSettings);
	}
	
	LocalRedirect(\Bitrix\Main\Context::getCurrent()->getServer()->getRequestUri());
}

$APPLICATION->SetTitle(Loc::getMessage('MEIJI_SOLUTION_SMTP_ADMIN_PAGE_TITLE'));

$arSettings = \Meiji\BxSmtp\Mailer::getAdminFields();

$tabControl = new CAdminTabControl('tabControl', [
	[
		'DIV'   => 'edit1',
		'TAB'   => Loc::getMessage('MEIJI_SOLUTION_SMTP_ADMIN_PAGE_TAB1_NAME'),
		'TITLE' => Loc::getMessage('MEIJI_SOLUTION_SMTP_ADMIN_PAGE_TAB1_TITLE')
	]
], false, false);

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_after.php');

/** @todo: Add test sending functional with debugging */
/*
$res = Bitrix\Main\Mail\Event::send([
	"EVENT_NAME" => "FEEDBACK_FORM",
	"LID"        => "s1",
	"C_FIELDS"   => [
		'EMAIL_TO' => 'iksanov@meiji.media',
		'TEXT' => 'thats it'
	]
]);
*/

?>
    <form method="POST" action="<? echo $APPLICATION->GetCurPage() ?>?lang=<? echo htmlspecialcharsbx(LANG) ?>"
          name="meiji_smtp1">
		<?
		$tabControl->Begin();
		$tabControl->BeginNextTab();
		?>
		<? foreach ($arSettings as $name => $arSetting) : ?>

            <tr>
                <td width="30%"><?= $arSetting['NAME'] ?></td>
                <td width="70%">
					<? if ('L' == $arSetting['TYPE']) : ?>
                        <select name="meiji_smtp[<?= $name ?>]">
							<? foreach ($arSetting['VALUES'] as $arValue): ?>
                                <option value="<?= $arValue['VAR'] ?>" <?= ($arValue['ACTIVE'] ? 'selected' :
									''); ?>><?= $arValue['VALUE'] ?></option>
							<? endforeach; ?>
                        </select>
					<? elseif ('C' == $arSetting['TYPE']): ?>
                        <input type="checkbox" name="meiji_smtp[<?= $name ?>]" value="Y"<?= ($arSetting['ACTIVE'] ?
							'checked' : ''); ?>>
					<? else: ?>
                        <input type="<?= !empty($arSetting['S_TYPE'])?$arSetting['S_TYPE']:'text' ?>" name="meiji_smtp[<?= $name ?>]" size="30"
                               value="<?= $arSetting['VALUE'] ?>">
					<? endif; ?>
                </td>
            </tr>
		
		<? endforeach; ?>
		<?
		$tabControl->Buttons(array(
			'btnSave'   => false,
			'btnCancel' => false
		));
		$tabControl->End();
		?>
    </form>
<?
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_admin.php');
?>